import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import ReactLoading from 'react-loading';
import './App.scss';
import CssBaseline from '@material-ui/core/CssBaseline';
import browserHistory from './helpers/history'
import { ToastContainer,toast } from 'react-toastify';

toast.configure({
  autoClose: 2500,
  draggable: true,
  style: {
    fontSize: 17
  }
});

const loading = <div><ReactLoading type='spinningBubbles' color={'#2196f3'} height={'30'} width={'30'} /></div>;

const DefaultLayout = React.lazy(() => import('./pages/DefaultLayout'));
const LogIn = React.lazy(() => import('./pages/LogIn'));

function App() {

  return (
    <div id={'main-app'}>
    <Router history={browserHistory}>
      <CssBaseline/>
      <React.Suspense fallback={loading}>
        <Switch>
          <Route path="/user" name="Welcome" component={LogIn} />
          {/* <Route exact path="/login" name="Register" component={Register} /> */}
          {/* <Route exact path="/404" name="Page 404" component={Page404} />
        <Route exact path="/403" name="Page 403" component={Page403} />
        <Route exact path="/500" name="Page 500" component={Page500} /> */}
          <Route path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </React.Suspense>
      <ToastContainer/>
    </Router>
    </div>

  );
}

export default App;
