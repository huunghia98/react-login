import validator from '../validator';

describe('Test validator', () => {
  it('validate username', () => {
    const invalid_un = ['', 'a', 'abAd9', 'abadA9dfkasd&@'];
    const valid_un = ['hellowo09Ald', 'helA09'];
    for (let i of invalid_un) {
      expect(validator.validateUsername(i)).toBeFalsy();
    }
    for (let i of valid_un) {
      expect(validator.validateUsername(i)).toBeTruthy();
    }
  });
  it('validate password', function() {
    const invalid_pw = ['', 'a', 'abAd9qe', 'qweqeafdfAd', 'QEQRADFAF2', 'ADFAadvdfvac'];
    const valid_pw = ['hellowo09Ald', 'hedfaA09'];
    for (let i of invalid_pw) {
      expect(validator.validatePassword(i)).toBeFalsy();
    }
    for (let i of valid_pw) {
      expect(validator.validatePassword(i)).toBeTruthy();
    }
  });
  it('validate email', function() {
    const invalid_em = ['', 'a', 'abAd9adf.com.vn', 'adfaf&@gmail.com', 'abadA9@gmail', 'adfieua@gmail.com.ez.vl'];
    const valid_em = ['adfa1d@gmail.com', 'hihih5@teko.com.vn'];
    for (let i of invalid_em) {
      expect(validator.validateEmail(i)).toBeFalsy();
    }
    for (let i of valid_em) {
      expect(validator.validateEmail(i)).toBeTruthy();
    }
  });
  it('validate fullname', function() {
    const invalid_fn = ['', '@', 'Hello 9', 'Helo* Man', 'Hello  world'];
    const valid_fn = ['Chi Dau', 'Lao Hac', 'A B C'];
    for (let i of invalid_fn) {
      expect(validator.validateFullname(i)).toBeFalsy();
    }
    for (let i of valid_fn) {
      expect(validator.validateFullname(i)).toBeTruthy();
    }
  });
});



