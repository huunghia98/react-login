const usRegex = /^[a-z0-9A-Z]{6,}$/;
const pwRegex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;
const emRegex = /^[a-z][a-z0-9_\.]{5,32}@[a-z0-9]{2,}(\.[a-z0-9]{2,4}){1,2}$/;
const fnRegex = /^(([^~!@#$%^&*/+\-_(\')\",.\\\?\]\[:{}|<>\s0-9]+)\s?)+$/;

function validateUsername(username) {
  return username && usRegex.test(username);
}

function validatePassword(password) {
  return pwRegex.test(password);
}

function validateEmail(email) {
  return emRegex.test(email);
}

function validateFullname(fullname) {
  return fnRegex.test(fullname);
}

export default {
  validateUsername,
  validatePassword,
  validateEmail,
  validateFullname,
};
