function getError(res) {
  return res.response.data.message.split(':')[1];
}

export default getError
