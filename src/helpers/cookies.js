import Cookies from 'js-cookie';

const time = 1 / (24 * 12); //captcha counter life time

function removeAll() {
  Object.keys(Cookies.get()).forEach(function(cookieName) {
    Cookies.remove(cookieName);
  });
}

function checkCookie() {
  return Cookies.get('access_token');
}

function saveCookie(data) {
  let times = 1 / 48; //day
  Cookies.set('access_token', data.access_token, { expires: times });
}

function getCaptchaCounter() {
  return Number(Cookies.get('captcha') || 0);
}

function incrCaptchaCounter() {
  const value = Number(Cookies.get('captcha') || 0);
  Cookies.set('captcha', value + 1, { expires: time });
}

function resetCaptchaCounter() {
  Cookies.set('captcha', 0,{ expires: time });
}
function setCaptchaCounter(n) {
  Cookies.set('captcha',n,{ expires: time })
}

export default { removeAll, checkCookie, saveCookie, incrCaptchaCounter, resetCaptchaCounter,getCaptchaCounter,setCaptchaCounter };
