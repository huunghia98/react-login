import React, { Suspense, useEffect, useState } from 'react';
import { Switch, Route, Router, Redirect } from 'react-router-dom';
import SignIn from '../../components/SignIn';
import SignUp from '../../components/SignUp';
import Forgot from '../../components/Forgot'
import cookies from '../../helpers/cookies' ;

export default () => {
  let cookie = cookies.checkCookie();

  return (<div>
      {(cookie)&&<Redirect to='/'/>}
      <Switch>
        <Redirect exact from='/user' to='/user/signin'/>
        <Route path='/user/signin' name='Sign In' title='Sign In' component={SignIn}/>
        <Route path='/user/signup' exact name='Sign Up' title='Sign Up' component={SignUp}/>
        <Route path='/user/forgot' exact name='Forgot password' title='Forgot password' component={Forgot}/>
      </Switch>
    </div>
  );
};
