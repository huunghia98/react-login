import React, { Suspense } from 'react';
import { Link, Redirect, Route, Switch } from 'react-router-dom';
import {
  AppBar,
  Box,
  Toolbar,
  Typography,
  IconButton,
  MenuItem,
  MenuList,
  Popper,
  Paper,
  ClickAwayListener,
  SvgIcon,
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import brown from '@material-ui/core/colors/brown';
import routes from 'routes';
import cookies from '../../helpers/cookies';
import browserHistory from '../../helpers/history';
import ReactLoading from 'react-loading';

const loading = <div><ReactLoading type='spinningBubbles' color={'#2196f3'} height={'30'} width={'30'}/></div>;

export default () => {
  let cookie = cookies.checkCookie();
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  function handleToggle() {
    setOpen(prevOpen => !prevOpen);
  }

  function handleClose(event) {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  }

  function handleLogout(e) {
    handleClose(e);
    cookies.removeAll();
    browserHistory.push('/');
  }

  return (<div style={{backgroundColor: '#fafafa'}}>
      {
      (!cookie) && <Redirect to='/user/signin'/>
      }
      <AppBar position="static" style={{
        flexGrow: 1,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        boxShadow: '0px 1px 2px #666' ,
        maxHeight: '6vh',
      }}>
        <Toolbar>
          <IconButton edge="start" aria-label="Menu">
            <MenuIcon/>
          </IconButton>
          <Box style={{ flexGrow: 1 }}>
            <Typography variant="h6" component={Link} to={'/'} style={{ textDecoration: 'none', color: brown[700] }}>
              TEKO
            </Typography>
          </Box>
          <Box style={{ position: 'relative' }}>
            <IconButton
              aria-label="Account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleToggle}
              ref={anchorRef}
              style={{ alignContent: 'center', justifyContent: 'center', textAlign: 'center', alignItems: 'center' }}
            >
              <img src="https://img.icons8.com/material/24/000000/user-male-circle--v1.png"/>
            </IconButton>
            <Popper open={open} anchorEl={anchorRef.current} keepMounted transition disablePortal
                    style={{ position: 'absolute' }}>
              <Paper id="menu-list-grow" style={{ position: 'absolute', left: -100 }} hidden={!open}>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList>
                    <MenuItem onClick={handleClose} component={Link} to={'/change'}
                              style={{ flexGrow: 1, textDecoration: 'none', color: 'black' }}>Change
                      password</MenuItem>
                    <MenuItem onClick={handleLogout}>Logout</MenuItem>
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Popper>
          </Box>
        </Toolbar>
      </AppBar>
      <div>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map(route =>
              route.component ? (
                <Route
                  key={route.name}
                  path={route.path}
                  exact={route.exact}
                  name={route.name}
                  render={restProps => <route.component {...restProps} title={route.name}/>}
                />
              ) : null,
            )}
          </Switch>
        </Suspense>
      </div>
    </div>
  );
};
