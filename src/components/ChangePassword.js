import React, { useRef } from 'react';
import { Button, TextField, Box, Typography, Container, InputAdornment } from '@material-ui/core';
import { AccountCircle, Lock, LockOpen } from '@material-ui/icons';
import useStyles from '../helpers/loginStyle';
import { toast } from 'react-toastify';
import users from '../services/users';
import Cookies from 'js-cookie';
import getError from '../helpers/parseResponse';
import browserHistory from '../helpers/history';

export default function ChangePassword() {
  const classes = useStyles();
  const oldpass = useRef();
  const newpass = useRef();
  const confirm = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    if (newpass.current.value !== confirm.current.value) {
      toast.warn('Confirm new password not match');
      return false;
    }
    if (oldpass.current.value === newpass.current.value) {
      toast.warn('2 password must be different');
      return false;
    }
    users.changePassword(oldpass.current.value, newpass.current.value, Cookies.get('access_token'))
      .then(function(res) {
        if (res.data) {
          toast.success('Password changed successfully !!!');
          browserHistory.goBack();
        }
        else
          toast.error(getError(res));
      });
  }

  return (
    <Container maxWidth={'xs'}>
      <Box className={classes.paper} boxShadow={5} p={3} style={{
        backgroundColor: 'white',
      }}>
        <Typography component="h1" variant="h5">
          Change password
        </Typography>
        <form className={classes.form}>
          <TextField
            inputRef={oldpass}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <Lock/>
                </InputAdornment>
              ),
            }}
            variant="outlined"
            margin="normal"
            type='password'
            required
            fullWidth
            id="oldpass"
            label="Old password"
            name="oldpass"
            autoFocus
          />
          <Typography variant="caption" color="secondary" align="justify"/>
          <TextField
            inputRef={newpass}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockOpen/>
                </InputAdornment>
              ),
            }}
            variant="outlined"
            margin="normal"
            type='password'
            required
            fullWidth
            name="newpass"
            label="New password"
            id="newpass"
          />
          <TextField
            inputRef={confirm}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockOpen/>
                </InputAdornment>
              ),
            }}
            variant="outlined"
            margin="normal"
            type='password'
            required
            fullWidth
            name="confirm"
            label="Confirm new password"
            id="confirm"
          />
          <Box>
            <Button
              id={'submit'}
              onClick={handleSubmit}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Submit
            </Button>
          </Box>
        </form>
      </Box>
    </Container>
  );
}
