import React, { useEffect, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import { Avatar, Button, TextField, Grid, Typography, Container, Box,InputAdornment } from '@material-ui/core';
import { AccountCircle,VpnKey } from '@material-ui/icons';
import ReCAPTCHA from 'react-google-recaptcha';
import { GoogleLogin } from 'react-google-login';
import { toast } from 'react-toastify';
import browserHistory from '../../helpers/history';

import users from '../../services/users';
import useStyles from '../../helpers/loginStyle';
import cookies from '../../helpers/cookies';
import validator from '../../helpers/validator';
import getError from '../../helpers/parseResponse';

export default function SignIn() {
  const classes = useStyles();
  const captchaKey = process.env.REACT_APP_CAPTCHA_CLIENT_KEY;
  const ssoClientKey = process.env.REACT_APP_SSO_CLIENT_KEY;

  const username = useRef(null);
  const password = useRef(null);
  const recaptchaRef = useRef();
  const [captchaCounter, setCaptchaCounter] = useState(cookies.getCaptchaCounter());

  function captchaCounterCall(action) {
    switch (action) {
      case 'inc':
        setCaptchaCounter(captchaCounter + 1);
        cookies.incrCaptchaCounter();
        break;
      case 'reset':
        setCaptchaCounter(0);
        cookies.resetCaptchaCounter();
        break;
      case 'check':
        if (captchaCounter > 2)
          return true;
        return false;
      case 'reset':
        if (captchaCounter > 2)
          recaptchaRef.current.reset();
        break;
      case 'render':
        setCaptchaCounter(4);
        cookies.setCaptchaCounter(4);
        break;
      default:
        return false;
    }
  }

  function onResponseGoogleSuccess(googleUser) {
    let profile = googleUser.getBasicProfile();
    let id = googleUser.getAuthResponse().id_token;

    users.ssoLogin(id)
      .then(function(res) {
        cookies.saveCookie({ access_token: res.data.access_token });
        browserHistory.push('/');
      })
      .catch(e => {
        if (e.response){
          toast.warn(getError(e));
        }
        else toast.error('Sign in fail');
      });
  }

  function onResponseGoogleFail() {
    toast.error('Google Sign In fail ! ');
  }

  function handleSignIn(e) {
    e.preventDefault();
    let recaptchaValue = '';
    if (captchaCounterCall('check')) {
      recaptchaValue = recaptchaRef.current.getValue();
      if (!recaptchaValue) {
        toast.error('Captcha required');
        return;
      }
    }
    const us = username.current.value;
    const pw = password.current.value;
    if (!(validator.validateUsername(us) && validator.validatePassword(pw))) {
      toast.warn('Username and password not in format');
      return false;
    }
    users.getLogin(us, pw, recaptchaValue)
      .then(function(res) {
        captchaCounterCall('reset');
        if (res.data) {
          cookies.saveCookie(res.data);
          captchaCounterCall('reset');
          toast.success('Sign in successfully !!!');
          browserHistory.push('/')
          return true;
        }
        if (res.response) {
          if (getError(res).includes('Captcha')) {
            captchaCounterCall('render');
            return false;
          }
          toast.error(getError(res));
          captchaCounterCall('inc');
          return false;
        }
        toast.error('Sorry. Server inactive');
      });
  }

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <Box className={classes.paper} boxShadow={5} p={3}>
          <Link to='/user/signin' style={{ textDecoration: 'none' }}>
            <Avatar className={classes.avatar}>
              Teko
            </Avatar>
          </Link>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form}>
            <TextField
              inputRef={username}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
            />
            <Typography variant="caption" color="secondary" align="justify"/>
            <TextField
              inputRef={password}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Typography variant="caption" color="secondary" align="justify"/>
            {captchaCounter > 2 &&
            <Box style={{ display: 'flex', alignContent: 'center', justifyContent: 'center', marginTop: '3vh' }}>
              <ReCAPTCHA
                ref={recaptchaRef}
                sitekey={captchaKey}
              />
            </Box>
            }
            <Button
              id={'submit'}
              onClick={handleSignIn}
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container style={{ marginTop: '2vh' }}>
              <Grid item xs>
                <Link to='forgot' variant={'body2'} style={{ textDecoration: 'none' }}>
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link to='signup' variant={'body2'} style={{ textDecoration: 'none' }}>
                  {'Don\'t have an account? Sign Up'}
                </Link>
              </Grid>
            </Grid>
            <Box style={{
              display: 'flex',
              flexGrow: 1,
              alignContent: 'center',
              justifyContent: 'center',
              marginTop: '4vh',
            }}>
              <GoogleLogin
                clientId={ssoClientKey}
                buttonText="Continue with Google"
                onSuccess={onResponseGoogleSuccess}
                onFailure={onResponseGoogleFail}
                cookiePolicy={'single_host_origin'}
                style={{ flexGrow: 1 }}
              />
            </Box>
          </form>
        </Box>
      </Container>
    </div>
  );
}
