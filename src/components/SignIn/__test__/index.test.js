import React from 'react';
import SignIn from '../';
import { BrowserRouter } from 'react-router-dom';
import { mount, shallow } from 'enzyme';
import { mountToJson, shallowToJson } from 'enzyme-to-json';
import users from '../../../services/users';

describe('SignIn', function() {
  afterEach(() => {
    jest.restoreAllMocks();
  });
  it('should call api with params when click signin button', function() {
    let component = mount(<BrowserRouter><SignIn/></BrowserRouter>);
    // expect(mountToJson(component)).toMatchSnapshot();

    let getLogin = jest.spyOn(users, 'getLogin').mockReturnValue(Promise.resolve({ data: 'hello' }));
    component.find('input#username').getDOMNode().value = 'hnnghia2';
    component.find('input#password').getDOMNode().value = 'Nghia123';
    component.find('button#submit').simulate('click');


    expect(getLogin).toHaveBeenCalledWith('hnnghia2', 'Nghia123','');

  });
  // it('should call api with param when click sso google button', function() {
  //   let component = mount(<BrowserRouter><SignIn/></BrowserRouter>);
  //
  //   let ssoLogin = jest.spyOn(users,'ssoLogin')
  // });
});
