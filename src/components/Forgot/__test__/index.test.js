import React from 'react';
import Forgot from '../';
import { BrowserRouter } from 'react-router-dom';
import { mount, shallow } from 'enzyme';
import { mountToJson, shallowToJson } from 'enzyme-to-json';
import users from '../../../services/users';

jest.disableAutomock();
describe('Forgot', function() {
  afterEach(()=>{
    jest.restoreAllMocks();
  })
  it('should call api with params when click forgot button', function() {
    let component = mount(<BrowserRouter><Forgot/></BrowserRouter>);
    // expect(mountToJson(component)).toMatchSnapshot()

    let forgotPassword = jest.spyOn(users,'forgotPassword').mockReturnValue(Promise.resolve({data:'hello'}));
    component.find('input#username').getDOMNode().value = 'hnnghia2';
    component.find('input#email').getDOMNode().value = 'hnnghia2@gmail.com';
    component.find('button#submit').simulate('click');

    expect(forgotPassword).toHaveBeenCalledWith('hnnghia2','hnnghia2@gmail.com');
  });
});
