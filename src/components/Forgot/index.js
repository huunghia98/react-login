import React, { useRef } from 'react';
import {Link} from 'react-router-dom';
import {Avatar,Button,TextField,Grid,Box,Typography,Container,InputAdornment} from '@material-ui/core';
import { AccountCircle, Email } from '@material-ui/icons';

import useStyles from '../../helpers/loginStyle';
import { toast } from 'react-toastify';
import users from '../../services/users';
import browserHistory from '../../helpers/history';

export default function SignIn() {
  const classes = useStyles();
  const username = useRef();
  const email = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    return users.forgotPassword(username.current.value, email.current.value)
      .then(function(res) {
        if (res.data) {
          toast.info('Success!! Please check email for new password !!!');
          browserHistory.push('/form/signin');
          return true;
        }
        toast.error('Username and email incorrect !!!');
      });
  }

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <Box className={classes.paper} boxShadow={5} p={3}>
          <Link to='/user/signin' style={{textDecoration:'none'}}>
          <Avatar className={classes.avatar}>
            Teko
          </Avatar>
          </Link>
          <Typography component="h1" variant="h5">
            Forgot password
          </Typography>
          <form className={classes.form}>
            <TextField
              inputRef={username}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
            />
            <Typography variant="caption" color="secondary" align="justify"/>
            <TextField
              inputRef={email}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Email />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="email"
              label="Email"
              id="email"
              autoComplete="email"
            />
            <Box>
              <Button
                onClick={handleSubmit}
                id={'submit'}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Submit
              </Button>
            </Box>
            <Grid container>
              <Grid item xs>
                <Link to='/user/signin' variant={'body2'} style={{textDecoration:'none'}}>
                  Sign in now !
                </Link>
              </Grid>
              <Grid item>
                <Link to='/user/signup' variant={'body2'} style={{textDecoration:'none'}}>
                  {'Don\'t have an account? Sign Up'}
                </Link>
              </Grid>
            </Grid>
          </form>
        </Box>
      </Container>
    </div>
  );
}
