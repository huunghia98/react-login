import React, { useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  Avatar,
  Button,
  TextField,
  FormControlLabel,
  Typography,
  Container,
  RadioGroup,
  Radio,
  FormControl,
  FormLabel,
  Box,
  InputAdornment
} from '@material-ui/core';
import { AccountCircle, VpnKey, Email, Face } from '@material-ui/icons';

import { toast } from 'react-toastify';
import users from '../../services/users';
import validator from '../../helpers/validator';
import browserHistory from '../../helpers/history';
import useStyles from '../../helpers/loginStyle';

function validate(username, password, fullname, email) {
  let bool = true;
  if (!validator.validateUsername(username)) {
    toast.warn('Username must be at least 6 characters');
    bool = false;
  }
  if (!validator.validatePassword(password)) {
    toast.warn('Password must be at least 8 characters with uppercase,lowwercase and number');
    bool = false;
  }
  if (!validator.validateEmail(email)) {
    toast.warn('Email invalid');
    bool = false;
  }
  if (!validator.validateFullname(fullname)) {
    toast.warn('Full name invalid');
    bool = false;
  }
  return bool;
}

export default function SignUp() {
  const classes = useStyles();

  const username = useRef(null);
  const password = useRef(null);
  const fullname = useRef(null);
  const email = useRef(null);
  const [gender, setGender] = useState('female');
  const [signup, setSignup] = useState('Sign up');

  function handleSignUp(e) {
    e.preventDefault();
    setSignup('...');
    if (!validate(username.current.value, password.current.value, fullname.current.value, email.current.value)) {
      setSignup('Sign up');
      return false;
    }
    users.register(username.current.value, password.current.value, fullname.current.value, email.current.value, gender)
      .then(function(res) {
        setSignup('Sign up');
        if (!res.data) {
          let noti = '';
          if (res.response) {
            noti = res.response.data.message.split(':')[1];
            toast.error(noti);
          }
          else toast.error('Sorry. Server not active');
          return false;
        }
        toast.success('Sign up successfully! Please check email to active account !');
        browserHistory.push('/user/signin');
      });
  }

  return (
    <div>
      <Container component="main" maxWidth="xs">
        <Box className={classes.paper} boxShadow={5} p={3}>
          <Link to='/user/signin' style={{ textDecoration: 'none' }}>
            <Avatar className={classes.avatar}>
              Teko
            </Avatar>
          </Link>
          <Typography component="h1" variant="h5">
            Sign up
          </Typography>
          <form className={classes.form}>
            <TextField
              inputRef={username}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircle />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="username"
              label="Username"
              name="username"
              autoComplete="username"
              autoFocus
            />
            <Typography variant="caption" color="secondary" align="justify"/>
            <TextField
              inputRef={password}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <VpnKey />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <TextField
              inputRef={email}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Email />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="email"
              label="Email"
              id="email"
              autoComplete="email"
            />
            <Typography variant="caption" color="secondary" align="justify"/>
            <TextField
              inputRef={fullname}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <Face />
                  </InputAdornment>
                ),
              }}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="fullname"
              label="Fullname"
              id="fullname"
              autoComplete="name"
            />
            <FormControl component={'fieldset'} style={{display:'flex',justifyContent:'center'}}>
              <FormLabel component={'legend'}>
                Gender
              </FormLabel>
              <RadioGroup
                id={'gender'}
                aria-label="Gender"
                name="gender"
                style={{ display: 'flex', flexDirection: 'row' }}
                value={gender}
                onChange={(e) => {
                  setGender(e.target.value);
                }}
              >
                <FormControlLabel value={'female'} control={<Radio/>} label="Female" defaultChecked/>
                <FormControlLabel value={'male'} control={<Radio/>} label="Male"/>
              </RadioGroup>
            </FormControl>
            <Box>
              <Button
                onClick={handleSignUp}
                id={'submit'}
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                {signup}
              </Button>
            </Box>
            <Box display={'flex'} justifyContent={'center'}>
              <Link to='/user/signin' variant={'body2'} style={{ textDecoration: 'none' }}>
                Have account? Sign In
              </Link>
            </Box>
          </form>
        </Box>
      </Container>
    </div>
  );
}
