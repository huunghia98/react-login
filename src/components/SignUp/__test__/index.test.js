import React from 'react';
import SignUp from '../';
import { BrowserRouter } from 'react-router-dom';
import { mount, shallow } from 'enzyme';
import { mountToJson, shallowToJson } from 'enzyme-to-json';
import users from '../../../services/users';

describe('Signup', function() {
  afterEach(()=>{
    jest.restoreAllMocks();
  })
  it('should call api with params when data valid and click signup button', function() {
    let component = mount(<BrowserRouter><SignUp/></BrowserRouter>);
    // expect(mountToJson(component)).toMatchSnapshot()

    const data = ['hnnghia2','Nghia123','Hello','hnnghia2@gmail.com','female'];
    function simulateData(data) {
      component.find('input#username').getDOMNode().value = data[0];
      component.find('input#password').getDOMNode().value = data[1];
      component.find('input#fullname').getDOMNode().value = data[2];
      component.find('input#email').getDOMNode().value = data[3];
      component.update();
      component.find('button#submit').simulate('click');
    }
    let register = jest.spyOn(users,'register').mockReturnValue(Promise.resolve({data:'hello'}));
    simulateData(data)
    // let u = component.find('input[type="radio"][value="male"]');
    // component.find('#gender').first().props.value = 'male';
    // component.find('#gender[name="gender"]').simulate('change',{target:{value: 'male'}});
    // let u = component.find('#gender[name="gender"]');
    expect(register).toHaveBeenCalledWith(...data);
  });
  it('should not call api when data invalid and click signup button', function() {
    let component = mount(<BrowserRouter><SignUp/></BrowserRouter>);
    function simulateData(data) {
      component.find('input#username').getDOMNode().value = data[0];
      component.find('input#password').getDOMNode().value = data[1];
      component.find('input#fullname').getDOMNode().value = data[2];
      component.find('input#email').getDOMNode().value = data[3];
      component.update();
      component.find('button#submit').simulate('click');
    }

    let register = jest.spyOn(users,'register').mockReturnValue(Promise.resolve({data:'hello'}));
    const invalid_data = [
      ['','Nghia123','Hello','hnnghia2@gmail.com','female'],
      ['hnnghia2','','Hello','hnnghia2@gmail.com','female'],
      ['hnnghia2','nghia0009','Hello','hnnghia2@gmail.com','female'],
      ['hnnghia2','Nghia123','h&','hnnghia2@gmail.com','female'],
      ['hnnghia2','Nghia123','&','hnnghia2@gmail.com','female'],
      ['hnnghia2','Nghia123','Hello','','female'],
      ['hnnghia2','Nghia123','Hello','a@gmail.com','female'],
      ['hnnghia2','Nghia123','Hello','aadfa@gmail','female'],
    ];
    for (let dt of invalid_data){
      simulateData(dt);
      expect(register).not.toBeCalled();
    }
  });
});
