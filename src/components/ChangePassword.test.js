import React from 'react';
import ChangePassword from './ChangePassword';
import { BrowserRouter } from 'react-router-dom';
import { mount, shallow } from 'enzyme';
import { mountToJson, shallowToJson } from 'enzyme-to-json';
import users from '../services/users';
import Cookies from 'js-cookie';

describe('Change password', function() {
  afterEach(()=>{
    jest.restoreAllMocks();
  })
  it('should call api with params when data valid and click submit change button', function() {
    let component = mount(<BrowserRouter><ChangePassword/></BrowserRouter>);
    // expect(mountToJson(component)).toMatchSnapshot()

    let changepw = jest.spyOn(users,'changePassword').mockReturnValue(Promise.resolve({data:'hello'}));
    let cookieGet = jest.spyOn(Cookies,'get').mockReturnValue('access_token_gi_do');

    component.find('input#oldpass').getDOMNode().value = 'Nghia12345';
    component.find('input#newpass').getDOMNode().value = 'Helloworld123';
    component.find('input#confirm').getDOMNode().value = 'Helloworld123';
    component.find('button#submit').simulate('click');


    expect(changepw).toHaveBeenCalledWith('Nghia12345','Helloworld123','access_token_gi_do');

  });
  it('should not call api when data invalid and click submit change button', function() {
    let component = mount(<BrowserRouter><ChangePassword/></BrowserRouter>);
    // expect(mountToJson(component)).toMatchSnapshot()

    let changepw = jest.spyOn(users,'changePassword').mockReturnValue(Promise.resolve({data:'hello'}));
    let cookieGet = jest.spyOn(Cookies,'get').mockReturnValue('access_token_gi_do');

    component.find('input#oldpass').getDOMNode().value = 'Nghia12345';
    component.find('input#newpass').getDOMNode().value = 'Helloworld123';
    component.find('input#confirm').getDOMNode().value = 'Hellowor125';
    component.find('button#submit').simulate('click');

    expect(changepw).not.toBeCalled();

    component.find('input#oldpass').getDOMNode().value = 'Nghia12345';
    component.find('input#newpass').getDOMNode().value = 'Nghia12345';
    component.find('input#confirm').getDOMNode().value = 'Nghia12345';
    component.find('button#submit').simulate('click');

    expect(changepw).not.toBeCalled();
  });
});
