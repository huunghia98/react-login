import ChangePassword from './components/ChangePassword';

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/change', exact: true, name: 'Change password', component: ChangePassword },
];

export default routes;
