import customAxios from './requests';
import { checkCookie } from '../helpers/cookies';

function getLogin(username, password, recaptcha='') {
  if (!recaptcha)
    return customAxios.post('/users/login', {
      'username': username,
      'password': password,
    })
      .then(function(res) {
        return res;
      })
      .catch(function(e) {
        return e;
      });
  else
    return customAxios.post('/users/login', {
      'username': username,
      'password': password,
      'captcha': recaptcha
    })
      .then(function(res) {
        return res;
      })
      .catch(function(e) {
        return e;
      });
}

function checkAuth(access_token) {
  let headers = {
    Authorization: 'Bearer ' + access_token,
  };
  return customAxios.get('/welcome', {
    headers: headers,
  }).then(function(res) {
    return res;
  })
    .catch(function(err) {
      console.log(err);
      return false;
    });
}

function register(username, password, fullname, email, gender) {
  return customAxios.post('/users/register', {
    'username': username,
    'password': password,
    'email': email,
    'fullname': fullname,
    'gender': gender,
  }).then(function(res) {
    return res;
  }).catch(function(err) {
    console.log(err);
    return err;
  });
}

function forgotPassword(username, email) {
  return customAxios.post('/users/forgot', {
    username: username,
    email: email,
  }).then(function(res) {
    if (res)
      return res;
    else return false;
  }).catch(function(err) {
    console.log(err);
    return err;
  });
}

function changePassword(oldpass, newpass, access_token) {
  let headers = {
    Authorization: 'Bearer ' + access_token,
  };
  return customAxios.post('/users/change', {
    oldpass: oldpass,
    newpass: newpass,
  }, {
    headers: headers,
  }).then(function(res) {
    return res;
  })
    .catch(function(err) {
      console.log(err);
      return err;
    });
}

function ssoLogin(id_token) {
  return customAxios.post('/users/sso',{'token':id_token})
}
export default {
  getLogin, register, forgotPassword, changePassword, ssoLogin
};
